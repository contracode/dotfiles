.DEFAULT_GOAL:=install

PACKAGES=$(wildcard */)

.PHONY: install
install:
	stow --target $(HOME) --verbose $(PACKAGES)

.PHONY: uninstall
uninstall:
	stow --target $(HOME) --verbose --delete $(PACKAGES)

.PHONY: update
update: pull
	stow --target $(HOME) --verbose --restow $(PACKAGES)

.PHONY: pull
pull:
	git pull
